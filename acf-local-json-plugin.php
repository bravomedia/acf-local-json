<?php
/*
Plugin Name: ACF Local JSON plugin
Plugin URI: 
Description: Put this file in a plugin folder and create an /acf directory inside the plugin, ie: /my-plugin/acf
Author: khromov
Version: 0.1
*/


if(!defined('ACF_LOCAL_JSON_PATH')) {
  define('ACF_LOCAL_JSON_PATH', __DIR__ . '/../../acf');
}

//Change ACF Local JSON save location to /acf folder inside this plugin
add_filter('acf/settings/save_json', function() {
  return ACF_LOCAL_JSON_PATH;
});
//Include the /acf folder in the places to look for ACF Local JSON files
add_filter('acf/settings/load_json', function( $paths ) {
  $paths[] = ACF_LOCAL_JSON_PATH;
  return $paths;
});
